------------------------------------------------------------------------
### Setup host

1. Set up VM with ubuntu 18.04.

2. Connect with e. g. `gcloud compute --project "audi-gan" ssh --zone "europe-west4-a" "everybody-dance-now" -- -L *:8088:163.172.48.117:1500 -o ServerAliveInterval=60 -o StrictHostKeyChecking=no`.

3. Install cuda10: https://askubuntu.com/questions/1077061/how-do-i-install-nvidia-and-cuda-drivers-into-ubuntu. reboot.

4. Install docker - https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository.

5. Reboot.

6. Install nvidia-docker2 as described on https://github.com/NVIDIA/nvidia-docker/ (quickstart)

------------------------------------------------------------------------
### Preprocessing

1. `ffmpeg -i file.mpg -r 10 frame%06d.jpg` for extraction with 10 FPS;
do for training and target video.

------------------------------------------------------------------------
### Pose estimation

------------------------------------------------------------------------
#### a. OpenPose

1. `sudo docker pull wenwu449/openpose:20181101-a8c23f`
2. `sudo nvidia-docker run -it -v /home/raphael/data/:/data --name openpose wenwu449/openpose:latest /bin/bash`
3. `mkdir /data/r10/output`
4. Run pose extraction to .json with `./build/examples/openpose/openpose.bin --image_dir /data/r10/ --write_json /data/r10/output --display 0 --render_pose 0 --face --hand &> openpose_log &`.
5. `exit && sudo docker stop openpose`

------------------------------------------------------------------------
#### b. DensePose

1. `sudo docker pull garyfeng/densepose`.
2. `sudo nvidia-docker run -it -v /home/raphael/data/:/data --name densepose garyfeng/densepose:latest /bin/bash`.
3. See https://github.com/facebookresearch/DensePose/blob/master/GETTING_STARTED.md for usage: Run with
```
python2 tools/infer_simple.py \
    --cfg configs/DensePose_ResNet101_FPN_s1x-e2e.yaml \
    --output-dir /data/r10/output/ \
    --image-ext jpg \
    --wts https://s3.amazonaws.com/densepose/DensePose_ResNet101_FPN_s1x-e2e.pkl \
    /data/r10/
```

------------------------------------------------------------------------
### vid2vid

1. `git clone https://github.com/NVIDIA/vid2vid.git`.
2. Build image with `cd vid2vid/docker && sudo nvidia-docker build -t vid2vid:CUDA9.0-py35 .`.
3. Move training images, test images, OpenPose and DensePose data into `vid2vid/datasets/pose`.
4. `cd vid2vid && sudo nvidia-docker run --rm -tid --ipc=host --shm-size 60G --name vid2vid -v $(pwd):/vid2vid --workdir=/vid2vid vid2vid:CUDA9.0-py35`.
6. Download further dependencies - see https://github.com/NVIDIA/vid2vid#training-with-pose-datasets; e. g. tensorflow for logging for tensorboard. Reinstall of flownet might be necessary. 

------------------------------------------------------------------------------
#### a. Training

1. `cd vid2vid`
2.  Train with
```nohup python train.py --name pose2body_256p \
--dataroot datasets/pose \
--dataset_mode posec  \
--input_nc 6 --num_D 2 \
--resize_or_crop  randomScaleHeight_and_scaledCrop  \
--loadSize 384  \
--fineSize 256 \
--gpu_ids 0,1,2,3  \
--batchSize 1  \
--max_frames_per_gpu 2 \
--niter 5  \
--niter_decay 5  \
--nThreads 10 \
--no_first_img  \
--n_frames_total 12  \
--max_t_step 4  \
--no_flow  \
--tf_log &
```

------------------------------------------------------------------------------
#### b. Inference

1. Infer with
```nohup python test.py --name pose2body_256p \
--dataroot datasets/pose  \
--dataset_mode pose \
--input_nc 6  \
--resize_or_crop scaleHeight  \
--loadSize 256  \
--no_first_img &
```

------------------------------------------------------------------------------
#### Misc. notes

* Additional face discriminator available (see https://github.com/NVIDIA/vid2vid/issues/8) with `----add_face_disc`; weight can be modified using face_weight in vid2vid_model_D.py. 